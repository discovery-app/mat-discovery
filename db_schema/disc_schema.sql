-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema discovery
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema discovery
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `discovery` DEFAULT CHARACTER SET latin1 ;
USE `discovery` ;

-- -----------------------------------------------------
-- Table `discovery`.`task`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`task` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` TEXT NULL DEFAULT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `created_by` TEXT NULL DEFAULT NULL,
  `start_date` DATETIME NULL DEFAULT NULL,
  `end_date` DATETIME NULL DEFAULT NULL,
  `updated_by` TEXT NULL DEFAULT NULL,
  `login_mode` TEXT NULL DEFAULT NULL,
  `account_id` MEDIUMINT UNSIGNED NULL DEFAULT NULL,
  `discovery_type` TEXT NULL DEFAULT NULL,
  `created_datetime` DATETIME NULL DEFAULT NULL,
  `scheduled_time` DATETIME NULL DEFAULT NULL,
  `execution_mode` TEXT NULL DEFAULT NULL,
  `url` TEXT NULL DEFAULT NULL,
  `port` TEXT NULL DEFAULT NULL,
  `username` TEXT NULL DEFAULT NULL,
  `password` TEXT NULL DEFAULT NULL,
  `type` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0 #1160
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`datacenter`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`datacenter` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `task_id` MEDIUMINT UNSIGNED NOT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `network_count` TEXT NULL DEFAULT NULL,
  `vcenter_host_count` INT UNSIGNED NULL DEFAULT NULL,
  `cluster_count` INT UNSIGNED NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `df_task_fk_1` (`task_id` ASC) ,
  CONSTRAINT `df_task_fk_1`
    FOREIGN KEY (`task_id`)
    REFERENCES `discovery`.`task` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #9
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`cluster`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`cluster` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `datacenter_id` INT UNSIGNED NULL DEFAULT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `vcenter_host_count` INT UNSIGNED NULL DEFAULT NULL,
  `effective_vcenter_host_count` INT UNSIGNED NULL DEFAULT NULL,
  `vm_count` INT UNSIGNED NULL DEFAULT NULL,
  `power_off_vm_count` INT UNSIGNED NULL DEFAULT NULL,
  `cpu_core_count` INT UNSIGNED NULL DEFAULT NULL,
  `cpu_thread_count` INT UNSIGNED NULL DEFAULT NULL,
  `effective_cpu_count` INT UNSIGNED NULL DEFAULT NULL,
  `total_cpu` INT UNSIGNED NULL DEFAULT NULL,
  `effective_memory` INT UNSIGNED NULL DEFAULT NULL,
  `total_memory` BIGINT UNSIGNED NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `datacenter_cluster_fk_1_idx` (`datacenter_id` ASC) ,
  CONSTRAINT `datacenter_cluster_fk_1`
    FOREIGN KEY (`datacenter_id`)
    REFERENCES `discovery`.`datacenter` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #9
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`vcenter_host`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`vcenter_host` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cluster_id` INT UNSIGNED NULL DEFAULT NULL,
  `ip` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `cpu_usage` TEXT NULL DEFAULT NULL,
  `host_count` INT UNSIGNED NULL DEFAULT NULL,
  `memory_usage` TEXT NULL DEFAULT NULL,
  `cpu_clock_speed` TEXT NULL DEFAULT NULL COMMENT 'usint is in MHz',
  `cpu_core_count` INT UNSIGNED NULL DEFAULT NULL,
  `cpu_model` TEXT NULL DEFAULT NULL,
  `cpu_thread_count` INT UNSIGNED NULL DEFAULT NULL,
  `interface_count` INT UNSIGNED NULL DEFAULT NULL,
  `uuid` TEXT NULL DEFAULT NULL,
  `vendor` TEXT NULL DEFAULT NULL,
  `model` TEXT NULL DEFAULT NULL,
  `hypervisor_version` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `vcenter_host_cluster_fk2_idx` (`cluster_id` ASC) ,
  CONSTRAINT `vcenter_host_cluster_fk2`
    FOREIGN KEY (`cluster_id`)
    REFERENCES `discovery`.`cluster` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`host`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`host` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `task_id` MEDIUMINT UNSIGNED NOT NULL,
  `ip` VARCHAR(15) NULL DEFAULT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `platform` TEXT NULL DEFAULT NULL,
  `operating_system` TEXT NULL DEFAULT NULL,
  `version` TEXT NULL DEFAULT NULL,
  `release_version` TEXT NULL DEFAULT NULL,
  `historic` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `job_entry` TINYINT UNSIGNED NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `reason` TEXT NULL DEFAULT NULL,
  `device_type` TEXT NULL DEFAULT NULL,
  `bios_configuration` TEXT NULL DEFAULT NULL,
  `system_date` TEXT NULL DEFAULT NULL,
  `instance_type` TEXT NULL DEFAULT NULL,
  `service_provider` TEXT NULL DEFAULT NULL,
  `memory` FLOAT(8,2) UNSIGNED NULL DEFAULT NULL,
  `stage` TINYINT NULL DEFAULT NULL,
  `device_identification_flag` TINYINT NULL DEFAULT NULL,
  `login_flag` TINYINT NULL DEFAULT NULL,
  `gateway_ip` TEXT NULL DEFAULT NULL,
  `community` TEXT NULL DEFAULT NULL,
  `instruction_set` TEXT NULL DEFAULT NULL,
  `sku` TEXT NULL DEFAULT NULL,
  `timezone` TEXT NULL DEFAULT NULL,
  `domain` TEXT NULL DEFAULT NULL,
  `uptime` TEXT NULL DEFAULT NULL,
  `installed_on` TEXT NULL DEFAULT NULL,
  `pagefile_size` TEXT NULL DEFAULT NULL,
  `internet_access` TEXT NULL DEFAULT NULL,
  `vcenter_host_id` INT UNSIGNED NULL DEFAULT NULL,
  `power_status` TEXT NULL DEFAULT NULL COMMENT 'This column is specific to vcenter VM\'s(vcenter IPs).',
  `start_date` DATETIME NULL DEFAULT NULL,
  `end_date` DATETIME NULL DEFAULT NULL,
  `revision` VARCHAR(50) NULL DEFAULT 'upgrade recommended',
  `dependents` SMALLINT UNSIGNED NULL DEFAULT NULL,
  `cpu_usage` TINYTEXT NULL DEFAULT NULL,
  `memory_usage` TINYTEXT NULL DEFAULT NULL,
  `storage_usage` TINYTEXT NULL DEFAULT NULL,
  `activity` VARCHAR(50) NULL DEFAULT 'moderate',
  `memory_type` TINYTEXT NULL DEFAULT NULL,
  `rdc` TEXT NULL DEFAULT NULL,
  `report_status` TINYINT NULL DEFAULT '0',
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `task_id_index` (`task_id` ASC) ,
  INDEX `idx_host_ip` (`ip` ASC) ,
  INDEX `host_vcenter_host_fk2_idx` (`vcenter_host_id` ASC) ,
  CONSTRAINT `host_ibfk_1`
    FOREIGN KEY (`task_id`)
    REFERENCES `discovery`.`task` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `host_vcenter_host_fk2`
    FOREIGN KEY (`vcenter_host_id`)
    REFERENCES `discovery`.`vcenter_host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #88837
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`action`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`action` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `category` TEXT NULL DEFAULT NULL,
  `reason` TEXT NULL DEFAULT NULL,
  `recommendation` TEXT NULL DEFAULT NULL,
  `complexity` TEXT NULL DEFAULT NULL,
  `criticality` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_action_host_fk1_idx` (`host_id` ASC) ,
  CONSTRAINT `fk_action_host_fk1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #584
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`application`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`application` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NULL DEFAULT NULL,
  `discovered_name` VARCHAR(250) NULL DEFAULT NULL,
  `app_code` TEXT NULL DEFAULT NULL,
  `app_owner` TEXT NULL DEFAULT NULL,
  `business_group` TEXT NULL DEFAULT NULL,
  `environment` TEXT NULL DEFAULT NULL,
  `business_critical` TEXT NULL DEFAULT NULL,
  `availability` TEXT NULL DEFAULT NULL,
  `performance` TEXT NULL DEFAULT NULL,
  `cloud_suitability` TEXT NULL DEFAULT NULL,
  `target_deployment` TEXT NULL DEFAULT NULL,
  `migration_strategy` TEXT NULL DEFAULT NULL,
  `integration_complexity` TEXT NULL DEFAULT NULL,
  `security` TEXT NULL DEFAULT NULL,
  `compliance` TEXT NULL DEFAULT NULL,
  `elasticity` TEXT NULL DEFAULT NULL,
  `utilization` TEXT NULL DEFAULT NULL,
  `maturity` TEXT NULL DEFAULT NULL,
  `migration_complexity` TEXT NULL DEFAULT NULL,
  `migration_priority` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `app_name_idx` (`name` ASC) ,
  UNIQUE INDEX `discovered_name_idx` (`discovered_name` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 0 #20
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`application_host_mapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`application_host_mapping` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `application_id` SMALLINT UNSIGNED NOT NULL,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `service_discovery_id` MEDIUMINT UNSIGNED NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_host_application_idx` (`host_id` ASC) ,
  INDEX `fk_application_idx` (`application_id` ASC) ,
  CONSTRAINT `fk_application`
    FOREIGN KEY (`application_id`)
    REFERENCES `discovery`.`application` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_host`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #58
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`application_recommendation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`application_recommendation` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `application_id` SMALLINT UNSIGNED NOT NULL,
  `cloud_provider` TEXT NULL DEFAULT NULL,
  `migration_priority` TEXT NULL DEFAULT NULL,
  `migration_suitability` TEXT NULL DEFAULT NULL,
  `migration_strategy` TEXT NULL DEFAULT NULL,
  `on_demand_price` FLOAT(10,2) NULL DEFAULT NULL,
  `reserved_1_year_price` FLOAT(10,2) NULL DEFAULT NULL,
  `reserved_3_year_price` FLOAT(10,2) NULL DEFAULT NULL,
  `recommended_on_demand_price` FLOAT(10,2) NULL DEFAULT NULL,
  `recommended_1_year_price` FLOAT(10,2) NULL DEFAULT NULL,
  `recommended_3_year_price` FLOAT(10,2) NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_application_recommendation_id_idx` (`application_id` ASC) ,
  CONSTRAINT `fk_application_recommendation_id`
    FOREIGN KEY (`application_id`)
    REFERENCES `discovery`.`application` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #91
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`certificate`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`certificate` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `path` TEXT NULL DEFAULT NULL,
  `certificate_name` TEXT NULL DEFAULT NULL,
  `issuer` TEXT NULL DEFAULT NULL,
  `not_after` DATETIME NULL DEFAULT NULL,
  `not_before` DATETIME NULL DEFAULT NULL,
  `serial_number` TEXT NULL DEFAULT NULL,
  `thumb_print` TEXT NULL DEFAULT NULL,
  `dns_name_list` TEXT NULL DEFAULT NULL,
  `subject` TEXT NULL DEFAULT NULL,
  `version` TEXT NULL DEFAULT NULL,
  `ps_provider` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `certificatecol` VARCHAR(45) NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `host_id` (`host_id` ASC) ,
  INDEX `primary_index_asdsa` (`id` ASC) ,
  CONSTRAINT `host_cert_details_ibfk_1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #71785
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`compliance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`compliance` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `criticality` TEXT NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `compliance_host_fk1_idx` (`host_id` ASC) ,
  CONSTRAINT `compliance_host_fk1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #13
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`compliance_lookup`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`compliance_lookup` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  `criticality` TEXT NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 0 #5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`core_services`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`core_services` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `service` VARCHAR(250) NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0 #37
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`cpu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`cpu` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `utilization_percent` FLOAT(5,2) UNSIGNED NULL DEFAULT NULL,
  `physical_processors` INT UNSIGNED NULL DEFAULT NULL COMMENT 'number of physical processor chips / cpu',
  `physical_cores` INT UNSIGNED NULL DEFAULT NULL COMMENT 'total number of cores in all processor chips',
  `logical_processors` INT UNSIGNED NULL DEFAULT NULL COMMENT 'number of logical processors available',
  `model_name` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `host_id_index` (`host_id` ASC) ,
  CONSTRAINT `host_cpu_details_ibfk_1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #1735
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`device_lookup`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`device_lookup` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `mib` TEXT NULL DEFAULT NULL,
  `device_type` TEXT NULL DEFAULT NULL,
  `platform` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0 #7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`docker_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`docker_info` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `version` TEXT NULL DEFAULT NULL,
  `build_time` TEXT NULL DEFAULT NULL,
  `os_type` TEXT NULL DEFAULT NULL,
  `root_dir` TEXT NULL DEFAULT NULL,
  `images` INT NULL DEFAULT NULL,
  `containers` INT NULL DEFAULT NULL,
  `containers_running` INT NULL DEFAULT NULL,
  `containers_paused` INT NULL DEFAULT NULL,
  `containers_stopped` INT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `host_docker_info_fk1_idx` (`host_id` ASC) ,
  CONSTRAINT `host_docker_info_fk1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #143
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`docker_image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`docker_image` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `docker_info_id` INT UNSIGNED NOT NULL,
  `containers` TEXT NULL DEFAULT NULL,
  `createdat` TEXT NULL DEFAULT NULL,
  `createdsince` TEXT NULL DEFAULT NULL,
  `digest` TEXT NULL DEFAULT NULL,
  `image_id` TEXT NULL DEFAULT NULL,
  `repository` TEXT NULL DEFAULT NULL,
  `sharedsize` TEXT NULL DEFAULT NULL,
  `size` TEXT NULL DEFAULT NULL,
  `tag` TEXT NULL DEFAULT NULL,
  `uniquesize` TEXT NULL DEFAULT NULL,
  `virtualsize` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `info_image_fk1_idx` (`docker_info_id` ASC) ,
  CONSTRAINT `info_image_fk1`
    FOREIGN KEY (`docker_info_id`)
    REFERENCES `discovery`.`docker_info` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #604
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`docker_container`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`docker_container` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `docker_image_id` INT UNSIGNED NULL DEFAULT NULL,
  `command` TEXT NULL DEFAULT NULL,
  `createdat` TEXT NULL DEFAULT NULL,
  `container_id` TEXT NULL DEFAULT NULL,
  `image` TEXT NULL DEFAULT NULL,
  `labels` TEXT NULL DEFAULT NULL,
  `localvolumes` TEXT NULL DEFAULT NULL,
  `mounts` TEXT NULL DEFAULT NULL,
  `names` TEXT NULL DEFAULT NULL,
  `networks` TEXT NULL DEFAULT NULL,
  `ports` TEXT NULL DEFAULT NULL,
  `runningfor` TEXT NULL DEFAULT NULL,
  `size` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `cpu_percentage` TEXT NULL DEFAULT NULL,
  `memory_usage` TEXT NULL DEFAULT NULL,
  `memory_limit` TEXT NULL DEFAULT NULL,
  `memory_percentage` TEXT NULL DEFAULT NULL,
  `net_in` TEXT NULL DEFAULT NULL,
  `net_out` TEXT NULL DEFAULT NULL,
  `block_in` TEXT NULL DEFAULT NULL,
  `block_out` TEXT NULL DEFAULT NULL,
  `pid` TEXT NULL DEFAULT NULL,
  `user_id` TEXT NULL DEFAULT NULL,
  `process_id` TEXT NULL DEFAULT NULL,
  `cpu_percent` TEXT NULL DEFAULT NULL,
  `image_id` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `image_container_fk1_idx` (`docker_image_id` ASC) ,
  CONSTRAINT `image_container_fk1`
    FOREIGN KEY (`docker_image_id`)
    REFERENCES `discovery`.`docker_image` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #148
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`error`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`error` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NULL DEFAULT NULL,
  `ip` TEXT NULL DEFAULT NULL,
  `type` TEXT NULL DEFAULT NULL,
  `error` TEXT NULL DEFAULT NULL,
  `time` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `error_fk_host_id_idx` (`host_id` ASC) ,
  CONSTRAINT `error_fk_host_id`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #6626
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`host_credentials`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`host_credentials` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `username` TEXT NULL DEFAULT NULL,
  `password` TEXT NULL DEFAULT NULL,
  `key` TEXT NULL DEFAULT NULL,
  `domain` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_cred_host_1_idx` (`host_id` ASC) ,
  CONSTRAINT `fk_cred_host_1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #89363
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`interface`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`interface` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `ip_address` TEXT NULL DEFAULT NULL,
  `gateway` TEXT NULL DEFAULT NULL,
  `additional_ip` TEXT NULL DEFAULT NULL,
  `packet_max_size` TEXT NULL DEFAULT NULL,
  `received_packets` TEXT NULL DEFAULT NULL,
  `sent_packets` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `mac_address` TEXT NULL DEFAULT NULL,
  `type` TEXT NULL DEFAULT NULL,
  `speed` TEXT NULL DEFAULT NULL,
  `ip_subnet` TEXT NULL DEFAULT NULL,
  `dhcp_enabled` TEXT NULL DEFAULT NULL,
  `dns_server` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `host_id_index` (`host_id` ASC) ,
  CONSTRAINT `host_interface_details_ibfk_1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #4377
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`ip`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`ip` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `task_id` MEDIUMINT UNSIGNED NOT NULL,
  `host_id` MEDIUMINT NULL DEFAULT NULL,
  `ip_list` TEXT NULL DEFAULT NULL,
  `type` TEXT NULL DEFAULT NULL,
  `region` TEXT NULL DEFAULT NULL,
  `data_center` TEXT NULL DEFAULT NULL,
  `line_of_business` TEXT NULL DEFAULT NULL,
  `project` TEXT NULL DEFAULT NULL,
  `environment` TEXT NULL DEFAULT NULL,
  `application` TEXT NULL DEFAULT NULL,
  `utilization` CHAR(1) NULL DEFAULT NULL,
  `utilization_interval` TINYINT UNSIGNED NULL DEFAULT NULL,
  `utilization_period` TINYINT UNSIGNED NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `task_id_index` (`task_id` ASC) ,
  CONSTRAINT `fk1`
    FOREIGN KEY (`task_id`)
    REFERENCES `discovery`.`task` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #7030
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`job_host_mapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`job_host_mapping` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `job_id` TEXT NULL DEFAULT NULL,
  `job_type` TEXT NULL DEFAULT NULL,
  `job_status` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0 #240
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`login_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`login_history` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `time` DATETIME NULL DEFAULT NULL,
  `user_name` TEXT NULL DEFAULT NULL,
  `server_name` TEXT NULL DEFAULT NULL,
  `server_ip` TEXT NULL DEFAULT NULL,
  `action` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `host_id_idx` (`host_id` ASC) ,
  CONSTRAINT `host_id`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #348090
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`memory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`memory` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `used` FLOAT(8,2) UNSIGNED NULL DEFAULT NULL,
  `free` FLOAT(8,2) UNSIGNED NULL DEFAULT NULL COMMENT 'Free memory is the amount of memory which is currently not used for anything. This number should be small, because memory which is not used is simply wasted.',
  `available` FLOAT(8,2) UNSIGNED NULL DEFAULT NULL COMMENT 'Available memory is the amount of memory which is available for allocation to a new process or to existing processes.',
  `total` FLOAT(8,2) UNSIGNED NULL DEFAULT NULL,
  `used_percent` FLOAT(5,2) UNSIGNED NULL DEFAULT NULL,
  `type` TEXT NULL DEFAULT NULL,
  `speed` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `host_id_index` (`host_id` ASC) ,
  CONSTRAINT `host_memory_details_ibfk_1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #1715
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`os_lookup`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`os_lookup` (
  `id` BIGINT NULL DEFAULT NULL,
  `operating_system` TEXT NULL DEFAULT NULL,
  `base_name` TEXT NULL DEFAULT NULL,
  `version` TEXT NULL DEFAULT NULL,
  `platform` TEXT NULL DEFAULT NULL,
  `cloud_provider` TEXT NULL DEFAULT NULL,
  `compatibility` TEXT NULL DEFAULT NULL,
  `available_image_link` TEXT NULL DEFAULT NULL,
  `available_image_name` TEXT NULL DEFAULT NULL,
  `support_date` DATETIME NULL DEFAULT NULL,
  `extended_support_date` TEXT NULL DEFAULT NULL,
  `recommended_os` TEXT NULL DEFAULT NULL,
  `recommendation` TEXT NULL DEFAULT NULL,
  `action` TEXT NULL DEFAULT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`os_recommendation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`os_recommendation` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `support_date` DATE NULL DEFAULT NULL,
  `cloud_provider` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `compatible` VARCHAR(3) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `message` VARCHAR(50) CHARACTER SET 'utf8' NULL DEFAULT 'Move as IAAS',
  `migration_strategy` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT 'rebuild',
  `migration_complexity` VARCHAR(50) CHARACTER SET 'utf8' NULL DEFAULT 'Moderate',
  `priority` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT 'migrate soon',
  `criticality` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT 'moderate',
  `compatibility_score` FLOAT(10,2) NULL DEFAULT NULL,
  `action` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `yearly_onpremise_cost` FLOAT(10,2) NULL DEFAULT NULL,
  `instance_type` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `on_demand_price` FLOAT(10,2) NULL DEFAULT NULL,
  `reserved_1_year_price` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `reserved_3_year_price` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `actual_storage_price` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `recommended_vcpu_count` MEDIUMINT UNSIGNED NULL DEFAULT NULL,
  `recommended_memory` MEDIUMINT UNSIGNED NULL DEFAULT NULL,
  `recommended_storage` MEDIUMINT UNSIGNED NULL DEFAULT NULL,
  `recommended_instance_type` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `recommended_on_demand_price` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `recommended_1_year_price` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `recommended_3_year_price` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `recommended_storage_price` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `no_upfront_fee_1_year` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `no_upfront_fee_3_year` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `partial_upfront_fee_1_year` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `partial_upfront_fee_3_year` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `full_upfront_fee_1_year` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `full_upfront_fee_3_year` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `os_rec_host_fk_1_idx` (`host_id` ASC) ,
  INDEX `os_recommendation_host_fk_1` (`host_id` ASC) ,
  CONSTRAINT `os_recommendation_host_fk_1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #4881
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`other_services`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`other_services` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `service` VARCHAR(250) NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0 #784
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`package`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`package` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `version` TEXT NULL DEFAULT NULL,
  `repository` TEXT NULL DEFAULT NULL,
  `basedir` TEXT NULL DEFAULT NULL,
  `category` TEXT NULL DEFAULT NULL,
  `arch` TEXT NULL DEFAULT NULL,
  `vendor` TEXT NULL DEFAULT NULL,
  `insdate` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `host_id_index` (`host_id` ASC) ,
  CONSTRAINT `host_package_details_ibfk_1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #8022391
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`port`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`port` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `local_address` VARCHAR(15) NULL DEFAULT NULL,
  `foreign_address` VARCHAR(60) NULL DEFAULT NULL,
  `number` TEXT NULL DEFAULT NULL,
  `foreign_port` TEXT NULL DEFAULT NULL,
  `direction` TEXT NULL DEFAULT NULL,
  `process_id` TEXT NULL DEFAULT NULL,
  `local_service` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `used_interface` TEXT NULL DEFAULT NULL,
  `service` TEXT NULL DEFAULT NULL,
  `connected_service` TEXT NULL DEFAULT NULL,
  `protocol` TEXT NULL DEFAULT NULL,
  `cpu_time` TEXT NULL DEFAULT NULL,
  `cpu_percent` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `recv_queue` TEXT NULL DEFAULT NULL,
  `send_queue` TEXT NULL DEFAULT NULL,
  `memory_percent` FLOAT(10,2) UNSIGNED NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `host_id_index` (`host_id` ASC) ,
  INDEX `idx_local_address` (`local_address` ASC) ,
  INDEX `idx_foreign_address` (`foreign_address` ASC) ,
  CONSTRAINT `host_port_details_ibfk_1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #180682
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`raw_env_variables`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`raw_env_variables` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `value` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_env_variables_host_idx` (`host_id` ASC) ,
  CONSTRAINT `fk_env_variables_host`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #8034
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`raw_process`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`raw_process` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `user` TEXT NULL DEFAULT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `process_name` TEXT NULL DEFAULT NULL,
  `product` TEXT NULL DEFAULT NULL,
  `path` TEXT NULL DEFAULT NULL,
  `cpu_percent` TEXT NULL DEFAULT NULL,
  `memory_percent` TEXT NULL DEFAULT NULL,
  `file_version` TEXT NULL DEFAULT NULL,
  `product_version` TEXT NULL DEFAULT NULL,
  `process_id` TEXT NULL DEFAULT NULL,
  `total_processor_time` TEXT NULL DEFAULT NULL,
  `start_time` TEXT NULL DEFAULT NULL,
  `vsz` TEXT NULL DEFAULT NULL,
  `rss` TEXT NULL DEFAULT NULL,
  `tty` TEXT NULL DEFAULT NULL,
  `stat` TEXT NULL DEFAULT NULL,
  `command` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_raw_process_host_idx` (`host_id` ASC) ,
  CONSTRAINT `fk_raw_process_host`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #109784
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`raw_programs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`raw_programs` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `publisher` TEXT NULL DEFAULT NULL,
  `install_location` TEXT NULL DEFAULT NULL,
  `version` TEXT NULL DEFAULT NULL,
  `display_version` TEXT NULL DEFAULT NULL,
  `version_major` TEXT NULL DEFAULT NULL,
  `version_minor` TEXT NULL DEFAULT NULL,
  `install_date` TEXT NULL DEFAULT NULL,
  `install_source` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_raw_programs_host_idx` (`host_id` ASC) ,
  CONSTRAINT `fk_raw_programs_host`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #1187
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`raw_services`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`raw_services` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `display_name` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `dependent_services` TEXT NULL DEFAULT NULL,
  `services_depended_on` TEXT NULL DEFAULT NULL,
  `service_type` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_raw_services_host_idx` (`host_id` ASC) ,
  CONSTRAINT `fk_raw_services_host`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #12585
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`raw_uptime`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`raw_uptime` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `command` TEXT NULL DEFAULT NULL,
  `user` TEXT NULL DEFAULT NULL,
  `caption` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_raw_uptime_host_idx` (`host_id` ASC) ,
  CONSTRAINT `fk_raw_uptime_host`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #53
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`recommendation_report`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`recommendation_report` (
  `host_id` INT NULL DEFAULT NULL,
  `support_date` TEXT NULL DEFAULT NULL,
  `cloud_provider` TEXT NULL DEFAULT NULL,
  `compatible` TEXT NULL DEFAULT NULL,
  `message` TEXT NULL DEFAULT NULL,
  `migration_strategy` TEXT NULL DEFAULT NULL,
  `migration_complexity` TEXT NULL DEFAULT NULL,
  `priority` TEXT NULL DEFAULT NULL,
  `criticality` TEXT NULL DEFAULT NULL,
  `compatibility_score` INT NULL DEFAULT NULL,
  `action` TEXT NULL DEFAULT NULL,
  `yearly_onpremise_cost` INT NULL DEFAULT NULL,
  `instance_type` TEXT NULL DEFAULT NULL,
  `on_demand_price` INT NULL DEFAULT NULL,
  `reserved_1_year_price` INT NULL DEFAULT NULL,
  `reserved_3_year_price` INT NULL DEFAULT NULL,
  `actual_storage_price` INT NULL DEFAULT NULL,
  `recommended_vcpu_count` INT NULL DEFAULT NULL,
  `recommended_memory` INT NULL DEFAULT NULL,
  `recommended_storage` INT NULL DEFAULT NULL,
  `recommended_instance_type` TEXT NULL DEFAULT NULL,
  `recommended_on_demand_price` INT NULL DEFAULT NULL,
  `recommended_1_year_price` INT NULL DEFAULT NULL,
  `recommended_3_year_price` INT NULL DEFAULT NULL,
  `recommended_storage_price` INT NULL DEFAULT NULL,
  `no_upfront_fee_1_year` TEXT NULL DEFAULT NULL,
  `no_upfront_fee_3_year` TEXT NULL DEFAULT NULL,
  `partial_upfront_fee_1_year` TEXT NULL DEFAULT NULL,
  `partial_upfront_fee_3_year` TEXT NULL DEFAULT NULL,
  `full_upfront_fee_1_year` TEXT NULL DEFAULT NULL,
  `full_upfront_fee_3_year` TEXT NULL DEFAULT NULL,
  `db_created_datetime` TEXT NULL DEFAULT NULL,
  `db_updated_datetime` TEXT NULL DEFAULT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`report`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`report` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` TEXT NULL DEFAULT NULL,
  `format` TEXT NULL DEFAULT NULL,
  `location` TEXT NULL DEFAULT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `account_id` MEDIUMINT UNSIGNED NULL DEFAULT NULL,
  `task_id` MEDIUMINT UNSIGNED NULL DEFAULT NULL,
  `host_id` MEDIUMINT UNSIGNED NULL DEFAULT NULL,
  `cloud_provider` TEXT NULL DEFAULT NULL,
  `reason` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `report_job_entry` TINYINT UNSIGNED NULL DEFAULT NULL,
  `created` TEXT NULL DEFAULT NULL,
  `file_name` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0 #537
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`scheduled`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`scheduled` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NULL DEFAULT NULL,
  `taskname` TEXT NULL DEFAULT NULL,
  `task_to_run` TEXT NULL DEFAULT NULL,
  `author` TEXT NULL DEFAULT NULL,
  `run_as_user` TEXT NULL DEFAULT NULL,
  `scheduled_task_state` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `last_run_time` TEXT NULL DEFAULT NULL,
  `next_run_time` TEXT NULL DEFAULT NULL,
  `schedule_type` TEXT NULL DEFAULT NULL,
  `start_time` TEXT NULL DEFAULT NULL,
  `start_date` TEXT NULL DEFAULT NULL,
  `end_date` TEXT NULL DEFAULT NULL,
  `command` TEXT NULL DEFAULT NULL,
  `minute` TEXT NULL DEFAULT NULL,
  `hour` TEXT NULL DEFAULT NULL,
  `day_of_the_month` TEXT NULL DEFAULT NULL,
  `month_of_the_year` TEXT NULL DEFAULT NULL,
  `day_of_the_week` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_sche_host_fk1_idx` (`host_id` ASC) ,
  CONSTRAINT `fk_sche_host_fk1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #6795
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`service` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `status` VARCHAR(4) NULL DEFAULT NULL,
  `type` VARCHAR(50) NULL DEFAULT 'Others',
  `path` TEXT NULL DEFAULT NULL,
  `discovery_id` TEXT NULL DEFAULT NULL,
  `version` TEXT NULL DEFAULT NULL,
  `port` TEXT NULL DEFAULT NULL,
  `cpu_percent` TEXT NULL DEFAULT NULL,
  `memory_percent` TEXT NULL DEFAULT NULL,
  `core_service` TINYINT NULL DEFAULT NULL,
  `comment` TEXT NULL DEFAULT NULL,
  `sid` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `service_fk_1_idx` (`host_id` ASC) ,
  CONSTRAINT `service_fk_1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #33900
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`service_list`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`service_list` (
  `Name` TEXT NULL DEFAULT NULL,
  `ServiceType` TEXT NULL DEFAULT NULL,
  `Status` TEXT NULL DEFAULT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`service_lookup`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`service_lookup` (
  `id` DOUBLE NULL DEFAULT NULL,
  `software` TEXT NULL DEFAULT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `version` TEXT NULL DEFAULT NULL,
  `type` TEXT NULL DEFAULT NULL,
  `cloud_provider` DOUBLE NULL DEFAULT NULL,
  `recommended_service` TEXT NULL DEFAULT NULL,
  `support_date` TEXT NULL DEFAULT NULL,
  `extended_support_date` DATETIME NULL DEFAULT NULL,
  `recommendation` DOUBLE NULL DEFAULT NULL,
  `action` TEXT NULL DEFAULT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`service_port`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`service_port` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `service_name` TEXT NULL DEFAULT NULL,
  `port` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0 #7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`service_recommendation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`service_recommendation` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `service_id` MEDIUMINT UNSIGNED NOT NULL,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `support_date` DATE NULL DEFAULT NULL,
  `message` VARCHAR(50) NULL DEFAULT 'Move as IAAS',
  `eosl` VARCHAR(20) NULL DEFAULT NULL,
  `criticality` VARCHAR(20) NULL DEFAULT 'Moderate',
  `migration_complexity` VARCHAR(50) NULL DEFAULT 'Moderate',
  `action` TEXT NULL DEFAULT NULL,
  `move_priority` VARCHAR(20) NULL DEFAULT 'Migrate soon',
  `migration_strategy` VARCHAR(20) NULL DEFAULT 'Refactor',
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE INDEX `idx_service_recommendation_id` (`id` ASC) ,
  INDEX `service_rec_fk1_idx` (`host_id` ASC) ,
  INDEX `fk2_service_rec_service_idx` (`service_id` ASC) ,
  CONSTRAINT `service_recommendation_fk_1`
    FOREIGN KEY (`service_id`)
    REFERENCES `discovery`.`service` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #6712
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`storage`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`storage` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `disk` TEXT NULL DEFAULT NULL,
  `type` TEXT NULL DEFAULT NULL,
  `partition` TEXT NULL DEFAULT NULL,
  `logical_volume` TEXT NULL DEFAULT NULL,
  `owner` TEXT NULL DEFAULT NULL,
  `group` TEXT NULL DEFAULT NULL,
  `permissions` TEXT NULL DEFAULT NULL,
  `file_system` TEXT NULL DEFAULT NULL,
  `total` FLOAT(8,2) UNSIGNED NULL DEFAULT NULL,
  `used` FLOAT(8,2) UNSIGNED NULL DEFAULT NULL,
  `available` FLOAT(8,2) UNSIGNED NULL DEFAULT NULL,
  `used_percent` FLOAT(5,2) UNSIGNED NULL DEFAULT NULL,
  `mounted` TEXT NULL DEFAULT NULL,
  `model` TEXT NULL DEFAULT NULL,
  `boot_partition` TEXT NULL DEFAULT NULL,
  `logical_disk` TEXT NULL DEFAULT NULL,
  `volume_serial_number` TEXT NULL DEFAULT NULL,
  `compressed` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `host_id_index` (`host_id` ASC) ,
  CONSTRAINT `host_storage_details_ibfk_1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #9223
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`user_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`user_group` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `group_name` TEXT NULL DEFAULT NULL,
  `user_id` TEXT NULL DEFAULT NULL,
  `username` TEXT NULL DEFAULT NULL,
  `home_directory` TEXT NULL DEFAULT NULL,
  `last_password_change` TEXT NULL DEFAULT NULL,
  `account_expiry_date` TEXT NULL DEFAULT NULL,
  `min_days_for_pwd_change` TEXT NULL DEFAULT NULL,
  `max_days_for_pwd_change` TEXT NULL DEFAULT NULL,
  `warning_days_for_pwd_change` TEXT NULL DEFAULT NULL,
  `password_inactive` TEXT NULL DEFAULT NULL,
  `password_status` TEXT NULL DEFAULT NULL,
  `password_expiry_date` TEXT NULL DEFAULT NULL,
  `last_login` TEXT NULL DEFAULT NULL,
  `group_description` TEXT NULL DEFAULT NULL,
  `user_type` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `host_id_index` (`host_id` ASC) ,
  CONSTRAINT `host_user_groups_ibfk_1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #5701
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`user_port`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`user_port` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `task_id` MEDIUMINT UNSIGNED NOT NULL,
  `name` TEXT NULL DEFAULT NULL,
  `port` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `task_id_index` (`task_id` ASC) ,
  CONSTRAINT `host_user_port_input_ibfk_1`
    FOREIGN KEY (`task_id`)
    REFERENCES `discovery`.`task` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`utilization`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`utilization` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` MEDIUMINT UNSIGNED NOT NULL,
  `executiontime` TEXT NULL DEFAULT NULL,
  `memory_percent` FLOAT(5,2) UNSIGNED NULL DEFAULT NULL,
  `cpu_percent` FLOAT(5,2) UNSIGNED NULL DEFAULT NULL,
  `storage_percent` FLOAT(5,2) UNSIGNED NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `host_id_index` (`host_id` ASC) ,
  CONSTRAINT `host_utilization_details_ibfk_1`
    FOREIGN KEY (`host_id`)
    REFERENCES `discovery`.`host` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0 #21752
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discovery`.`validation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`validation` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `validation_task_id` INT UNSIGNED NOT NULL,
  `ip` TEXT NULL DEFAULT NULL,
  `community` TEXT NULL DEFAULT NULL,
  `username` TEXT NULL DEFAULT NULL,
  `password` TEXT NULL DEFAULT NULL,
  `ping` TEXT NULL DEFAULT NULL,
  `telnet` TEXT NULL DEFAULT NULL,
  `login` TEXT NULL DEFAULT NULL,
  `privilege` TEXT NULL DEFAULT NULL,
  `cmd_execution` TEXT NULL DEFAULT NULL,
  `reason` TEXT NULL DEFAULT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `validation_task_fk_1_idx` (`validation_task_id` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 0 #2165
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `discovery`.`validation_task`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discovery`.`validation_task` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  `status` TEXT NULL DEFAULT NULL,
  `db_created_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_updated_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0 #13
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
