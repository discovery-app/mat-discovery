use discovery;
CREATE TABLE `enum` (
  `port_service_filter` TEXT NULL DEFAULT NULL,
  `port_number_filter` TEXT NULL DEFAULT NULL);
ALTER TABLE `enum`
ADD COLUMN `id` INT NOT NULL FIRST,
ADD PRIMARY KEY (`id`);
;
ALTER TABLE `enum`
CHANGE COLUMN `id` `id` INT NOT NULL AUTO_INCREMENT ;
ALTER TABLE `enum`
CHANGE COLUMN `port_number_filter` `port_number_filter` BIGINT NULL DEFAULT NULL ;
ALTER TABLE `enum`
DROP COLUMN `port_number_filter`,
CHANGE COLUMN `id` `name` VARCHAR(100) NOT NULL ,
CHANGE COLUMN `port_service_filter` `value` TEXT NULL ;
INSERT INTO `enum` (`name`, `value`) VALUES ('port_number_filter', '3389, 22, 5985, 5986, 443');
INSERT INTO `enum` (`name`, `value`) VALUES ('port_service_filter', 'explorer, svchost, MpCmdRun, git-remote-ht, Idle, MsMpEng, System, sshd, ssh, Unknown, unknown');
